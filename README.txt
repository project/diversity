# Drupal Diversity & Inclusion

## Welcome to DD&I

**Table of Contents**
- [What is DD&I](/drupaldiversity/administration/blob/admin-readme-updates/README.md#what-is-ddi "What is DD&I")
  - [How can I follow?](#how-can-i-follow "How can I follow?")
  - [I have Questions!](#i-have-questions "I have Questions!")
- [Ways to Contribute](#ways-to-contribute "Ways to Contribute")
    - [Weekly Meetings](#weekly-meetings "Weekly Meetings")
    - [Issue Queue](#issue-queue "Issue Queue")
- [Ongoing Projects](#ongoing-projects "Ongoing Projects")
- [Resources](#resources "Resources")
   - [Working Documents](#working-docs "Working Documents")

### What is DD&I

The Drupal Diversity and Inclusion Workgroup, also known as 'DD&I', is an independent collective of volunteers who advocate for greater diversity in the community and more inclusive events, help create safe spaces, and build the capacity of allies.

### How can I follow?

Come find us online!

* **Our Website** | https://drupaldiversity.github.io/
* **Twitter** | https://twitter.com/drupaldiversity
* **Instagram** | https://www.instagram.com/drupaldiversity/
* **Slack** | [Slack-Channel-Introduction.md](https://github.com/drupaldiversity/administration/blob/master/working-docs/slack-channel-introduction.md "Slack-Channel-Introduction.md")

### I have Questions!

(Link to FAQ doc)

You should also learn [about](https://drupaldiversity.github.io/about/ "about") us and take some time to [get involved](https://drupaldiversity.github.io/get-involved/ "get involved")!

## Ways to Contribute
We welcome all types of contributors - programmers, content managers, project support staff, and more. However you identify as a contributor and as part of the Drupal community, there is a place for you to get involved here.

### Weekly Meetings
- We meet in #diversity-inclusion channel in the Drupal slack every Thursday at 9am PST / 17:00pm UTC (The Time Zone Converter is handy for this). Get an invite to the Drupal slack and check out more
    - [Get an invite to the Drupal Slack](http://drupalslack.herokuapp.com/ "invite to drupal slack")
    - [Check out our calendar to see our scheduled meetings](https://calendar.google.com/calendar/embed?src=c0ovgjsi6p70huaunbe2a3mpj8%40group.calendar.google.com&ctz=America/Los_Angeles "ddi html calendar")

### Issue Queue

TBA


## Resources

TBA

### Working Docs

TBA
